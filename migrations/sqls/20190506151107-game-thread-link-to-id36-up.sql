UPDATE games SET thread_link=REGEXP_REPLACE(thread_link, 'https?://.*?/r/fakebaseball/comments/([a-z0-9]{6,7})/.*', '$1');

ALTER TABLE games
    CHANGE COLUMN thread_link id36 VARCHAR(7);
