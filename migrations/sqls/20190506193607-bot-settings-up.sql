CREATE TABLE IF NOT EXISTS bot_settings(
    setting_name VARCHAR(64) NOT NULL PRIMARY KEY,
    setting_value VARCHAR(128)
);
