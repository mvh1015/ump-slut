Ump Slut!
---

If you're here, I'll assume you already know what this program is for.
If you don't, it's a Discord bot for [/r/fakebaseball](https://reddit.com/r/fakebaseball).
If you're here to try and contribute to the code, welcome!
There are a few things you should know to get started.

### Base info
1. The program is written in TypeScript, so you'll need Node.js installed.
2. You should know how to properly submit a GitLab pull request.
3. In order to run the program locally you will need the following environment variables:
* `DISCORD_TOKEN`, the Discord Bot token. You'll need to create one at https://discordapp.com/developers
* `GOOGLE_API_KEY`, the Google Sheets API key to read from Google Sheets for player info. Make one by reading about it [here](https://developers.google.com/sheets/api/guides/authorizing) ~~or just steal one from Zum's site like I did for a bit~~.
* `REDDIT_PASSWORD`, optional and will have to be provided by me (not likely). This password allows you to read the reddit-based .claim codes. Shouldn't be necessary.
* `DEVELOPMENT=1`, **optional but recommended** to disable the DB and reddit interactions by default.

### Database
You will also need to create a file called `database.json` in the root directory of the project.
Within it, please enter the information to connect to your own testing database.

Example `database.json`:
```json
{
  "dev": {
    "driver": "mysql",
    "host": "ump-slut.com",
    "user": "ump-slut-dev",
    "password": "ump-slut-dev",
    "database": "ump-slut-dev",
    "multipleStatements": true
  }
}
```
For more info on configuring this file, [please read this documentation](https://db-migrate.readthedocs.io/en/latest/Getting%20Started/configuration/).
Once you have added this file, please use `npm run migrate` to create and populate the database.

**For MySQL 8+ users:** 
The Node.js mysql package does not yet support the new authentication method used by MySQL 8.
In order to use MySQL 8, you will need to create the user with the `mysql_native_password` authentication type.
That is,
```mysql
CREATE USER 'ump-slut-dev'@'%' IDENTIFIED WITH mysql_native_password BY 'mypassword';
```

If you would like to make a new migration,
please use the SQL-file migration format.
You can generate a new migration with empty SQL files by using `db-migrate create <migration-name> --sql-file`.

---

If you have any questions, feel free to DM me on Discord (Llamos#0001). 
Hopefully the structure is legible for you without explanation.
