import {Snowflake} from "discord.js";
import * as mysql from "promise-mysql";
import {getPlayerByRedditName, Player, Team} from "./players";
import {readFileSync} from "fs";
import {DEVELOPMENT_MODE} from "./important_constants";

let dbConfig = JSON.parse(readFileSync('database.json', 'utf8'));
let currentProfile = DEVELOPMENT_MODE ? 'dev' : 'prod';

const pool = mysql.createPool({
    connectionLimit: 10,
    host: dbConfig[currentProfile]['host'],
    user: dbConfig[currentProfile]['user'],
    password: dbConfig[currentProfile]['password'],
    database: dbConfig[currentProfile]['database'],
    ssl: {rejectUnauthorized: false}
});

/**
 * Finds a Discord snowflake by looking up a sheet-player
 * @param player The player to search
 * @return The snowflake found, or undefined
 */
export async function getSnowflake(player: Player): Promise<string | undefined> {
    // Search for snowflake by reddit name
    let res = await pool.query('SELECT person_snowflake FROM people WHERE reddit_name=?', [player.redditName]);
    if (res[0])
        return res[0].person_snowflake;

    // If none found, return undefined
    return undefined;
}

/**
 * Searches for a sheet-player by looking up a Discord snowflake
 * @param snowflake the snowflake to search
 * @return The player if found, or undefined
 */
export async function getPlayer(snowflake: Snowflake): Promise<Player | undefined> {
    // Search for reddit name by snowflake, then find the player off the sheet with that
    let res = await pool.query('SELECT reddit_name FROM people WHERE person_snowflake=?', [snowflake]);
    if (res[0])
        return await getPlayerByRedditName(res[0].reddit_name);

    // If we couldn't find the snowflake, return undefined
    return undefined;
}

/**
 * Checks whether or not a player is Discord Verified (has claimed their account)
 * @param player The player to check
 * @return true iff the player has verified, false otherwise
 */
export async function checkVerified(player: Player): Promise<boolean> {
    // Otherwise, check if that reddit name exists in DB
    let res = await pool.query('SELECT reddit_name FROM people WHERE reddit_name=?', [player.redditName]);
    return res[0] !== undefined;
}

/**
 * Claims a user's fakebaseball account, linking it to the Discord snowflake.
 * This also logs the method and claim time of the action.
 * It is expected that you have already checked that the snowflake is not already claimed,
 * otherwise this will throw an exception when the duplicate primary key verification fails.
 * @param player The sheet-player to claim
 * @param snowflake The snowflake to link player to
 * @param method The claim method
 */
// It's expected that you have already checked that a user is not claimed,
// otherwise this will throw an exception from a duplicate primary key violation
export async function claimUser(player: Player, snowflake: Snowflake, method: string) {
    // Insert the claim into the DB and log it
    await pool.query('INSERT INTO people (person_snowflake, reddit_name, claim_method) VALUES (?, ?, ?)', [snowflake, player.redditName, method]);
    console.log(`Claim: Player "${player.name}" was claimed by Discord user ${snowflake} on ${method}`);
}

export async function saveGameThread(away: Team, home: Team, id36: String) {
    if (id36.length > 7) {
        return 'ID36 length does not match.';
    }
    await pool.query('INSERT INTO games (away_team, home_team, id36) VALUES (?, ?, ?)', [away.tag, home.tag, id36]);
    console.log(`Saved game thread ${away.tag}@${home.tag} -> ${id36}`);
    return undefined;
}

//Get all information for a team's games. Optional parameter at 1 just in case you only need the latest game.
export async function getLatestGameThreadsForTeam(team: Team,numberOfResults = 1): Promise<any[]> {
    let res = await pool.query('SELECT * FROM games WHERE home_team = ? OR away_team = ? ORDER BY created DESC LIMIT ?', [team.tag, team.tag, numberOfResults]);
    
    const gameThreads = []
    
    //Gets either the number of results, or the number the parameter limits it to.
    let i = 0;
    for (let i = 0; i < res.length; i++)
    {
        gameThreads.push(res[i]);
        
        if (i >= numberOfResults)
            break;
    }

    if (res[0])
        return gameThreads;
    return undefined;
}



/**
 * This method grabs a boolean value from the DB's player settings.
 * It is an arbitrary key-value store, with each setting being unique and independent.
 * @param user The user to get a setting value of.
 * @param setting The setting to get. Max length of 128.
 * @param defaultValue The default value to return if the setting is not defined in the DB
 */
export async function getUserSetting(user: Snowflake, setting: string, defaultValue: boolean = undefined): Promise<boolean | undefined> {
    let res = await pool.query('SELECT setting_value FROM settings WHERE setting_user=? AND setting_key=?', [user, setting]);
    if (res[0])
        return res[0].setting_value;

    return defaultValue;
}

/**
 * This method stores a boolean value in the DB's player settings.
 * It is an arbitrary key-value store, with each setting being unique and independent.
 * This method will overwrite settings that already exist.
 * @param user The user to store a setting for.
 * @param setting The setting name to set. Max length of 128.
 * @param newValue The new value of the setting.
 */
export async function setUserSetting(user: Snowflake, setting: string, newValue: boolean) {
    await pool.query('INSERT INTO settings (setting_user, setting_key, setting_value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE setting_value=?', [user, setting, newValue, newValue]);
    console.log(`Updated player setting ${setting} for ${user} to ${newValue}`);
}



export async function setBotSetting(settingKey: string, settingValue: string) {
    await pool.query(`INSERT INTO bot_settings (setting_name, setting_value) VALUES (?, ?)`, [settingKey, settingValue]);
    console.log(`Set bot setting "${settingKey} => ${settingValue}"`);
}

export async function getBotSetting(settingKey: string): Promise<string | undefined> {
    let res = await pool.query(`SELECT setting_value FROM bot_settings WHERE setting_name=?`, [settingKey]);
    if (res[0])
        return res[0].setting_value;

    return undefined;
}

// endregion
