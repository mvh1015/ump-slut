import {GuildChannel, Message, RichEmbed} from "discord.js";
import {Command} from "./command";
import {saveGameThread} from "../database";
import {getTeam} from "../players";
import {DEVELOPMENT_MODE, REDDIT_ID36_FROM_URL_REGEX, SNOWFLAKE_ROLE_OOTC} from "../important_constants";

/**
 * This Command class should be extended by a sub-class.
 * It represents one possible command for the bot.
 * Only one instance is created per command,
 * so commands should not have any stateful actions.
 */
class AddGameCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    public async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        // make sure user is authorized
        let userAuthorized = DEVELOPMENT_MODE ||
            (msg.channel instanceof GuildChannel && msg.member.roles.has(SNOWFLAKE_ROLE_OOTC));
        if (!userAuthorized) {
            return "You can't do that!";
        }

        if (args.length < 3) {
            return "I need away, home, and link!";
        }

        const away = await getTeam(args[0]);
        if (typeof away === 'string') {
            return away;
        }
        const home = await getTeam(args[1]);
        if (typeof home === 'string') {
            return home;
        }

        let gameLink = args[2];
        let id36;
        if (gameLink.length === 6 || gameLink.length === 7) {
            id36 = gameLink;
        } else {
            let match = REDDIT_ID36_FROM_URL_REGEX.exec(gameLink);
            if (match[1] && (match[1].length === 6 || match[1].length === 7))
                id36 = match[1];
            else
                return `I couldn't get the ID36 out of the reddit link. Try providing it directly.`
        }

        const saveResult = await saveGameThread(away, home, id36);
        return typeof saveResult === 'undefined'
            ? `Saved game thread for ${away.tag} @ ${home.tag}! Players on these teams will be linked to this thread when they're pinged.`
            : `Couldn't save game thread: ${saveResult}`;

    }
}

export const AddGameCommand = new AddGameCommandClass();
