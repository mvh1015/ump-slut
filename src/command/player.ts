import {getPlayer as getPlayerFromSheet, Player} from "../players";
import * as Discord from "discord.js";
import {Command} from "./command";
import {RichEmbed} from "discord.js";
import {checkVerified, getPlayer as getPlayerFromDB} from "../database";

/**
 * Shows player info in a neat little embed card
 */
class PlayerCommandClass extends Command {

    public async handleCommand(args: string[], msg: Discord.Message): Promise<string | RichEmbed> {
        // We're gonna check the DB or the sheet, so set up a temp variable
        let player: Player;
        if (args.length < 1) {

            // If they didn't provide a name, check the DB for the current user's profile,
            // or tell them to provide a name/claim their account.
            player = await getPlayerFromDB(msg.author.id);
            if (player === undefined)
                return `I need a player to search for. Alternatively, claim your player with .claim to be the default.`;

        } else {

            // If they provided a name, find the player by search,
            // or tell them if we couldn't find the player
            let t = await getPlayerFromSheet(args.join(' '));
            if (typeof t === 'string')
                return t;
            player = t;

        }

        // Now that we have a player, we create the embed fields with all their info
        let fields = [];
        fields.push({
            inline: true,
            name: `Discord Name`,
            value: player.discordName.length > 0 ? player.discordName : "No Discord"
        });
        fields.push({inline: true, name: `Discord Verified`, value: await checkVerified(player) ? "Yes" : "No"});
        fields.push({inline: true, name: `Reddit Name`, value: player.redditName});
        fields.push({inline: true, name: `Primary Position`, value: player.primaryPos});
        fields.push({inline: true, name: `Secondary Position`, value: player.secondaryPos});
        fields.push({inline: true, name: `Extra Position`, value: player.tertiaryPos});
        fields.push({inline: true, name: `Team`, value: player.team.tag});
        fields.push({inline: true, name: `Batting Type`, value: player.battingType.name});
        fields.push({inline: true, name: `Hand`, value: player.rightHanded ? 'Right' : 'Left'});
        if (player.pitchingType.name !== 'Position') {
            fields.push({inline: true, name: `Pitching Type`, value: player.pitchingType.name});
            fields.push({inline: true, name: `Pitching Bonus`, value: player.pitchingBonus.name});
        }

        // Now create the embed and send it
        return new Discord.RichEmbed({
            title: player.name,
            color: player.team.color,
            // description: `**Discord Name:** ${searchResult.discordName.length > 0 ? searchResult.discordName : "No Discord"}\n**Reddit Name:** ${searchResult.redditName}\n**Team:** ${searchResult.team.tag}\n**Batting Type:** ${searchResult.battingType.name}\n**Pitching Type:** ${searchResult.pitchingType.name}\n**Pitching Bonus:** ${searchResult.pitchingBonus.name}`,
            fields: fields,
        })
    }
}

export const PlayerCommand = new PlayerCommandClass();
