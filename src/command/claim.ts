import {Command} from "./command";
import {Message, RichEmbed} from "discord.js";
import {getPlayer as getPlayerFromList} from "../players";
import {ClaimRequest, makeClaimRequest} from "../reddit_claim";
import {claimUser, getPlayer as getPlayerFromDB} from "../database";
import {EMBED_FOOTER} from "../important_constants";

/**
 * Allows players to claim their Discord account,
 * linking it to their /r/fakebaseball profile.
 * Once claimed, the user can still be pinged
 * even after changing usernames and discriminators.
 */
class ClaimCommandClass extends Command {
    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        // Make sure we have a name to claim
        if (args.length < 1)
            return `You need to provide a player name to claim.`;

        // Check if they have already claimed an account
        let currentClaim = await getPlayerFromDB(msg.author.id);
        if (currentClaim !== undefined)
            return `You have already claimed a player: ${currentClaim.name}.\n\nIf this is wrong, please contact Llamos#0001 immediately.`;

        // Find the player they would like to claim
        let playerQuery = args.join(' ');
        let player = await getPlayerFromList(playerQuery);

        // If the search failed, show the message
        if (typeof player === 'string')
            return player;

        // If their current Discord name doesn't match the one on the spreadsheet,
        // perform the two-part Reddit verification
        if (player.discordName !== `${msg.author.username}#${msg.author.discriminator}`) {
            // Set up for double sided auth
            let claimRequest = new ClaimRequest(msg.author.id, player.redditName);
            let claimCode = makeClaimRequest(claimRequest);

            // Create a nice embed message with everything they need to do to claim
            let instalink = `https://www.reddit.com/message/compose/?to=/u/SluttyUmps&subject=VERIFY&message=${encodeURIComponent(claimCode)}`;
            let message = `Your Discord name does not match the name registered to that player. In order to validate, please [click here](${instalink}).\n\nIf that does not work, please send a message to /u/SluttyUmps on Reddit with the following content:\n\n\`\`\`Subject: VERIFY\nMessage: ${claimCode}\`\`\``;
            let richMessage = new RichEmbed({
                title: 'Verification',
                footer: EMBED_FOOTER,
                description: message,
            });

            // Send the message and log it
            await msg.author.send(richMessage)
                .then(() => console.log(`Claim: Sent ${msg.author.username}#${msg.author.discriminator} a claim code for ${claimRequest.redditName}: ${claimCode}`));
            return `I wasn't able to verify over Discord. Please check your DMs.`;
        }

        // If their Discord did match, claim right away and let them know
        await claimUser(player, msg.author.id, 'Discord');
        return `Your account has been successfully claimed. Thank you!`;
    }
}

export const ClaimCommand = new ClaimCommandClass();
