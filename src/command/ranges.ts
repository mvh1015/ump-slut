import {Command} from "./command";
import {RichEmbed} from "discord.js";
import {getField, getPlayer} from "../players";
import {calculateRanges} from "../SwingStat";

/**
 * Provides swing ranges between two players on a specific field.
 */
class RangesCommandClass extends Command {

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        // Undo the args split by spaces, and instead split by semicolon
        let argsT = args.join(' ');
        args = argsT.split(';');

        // Make sure we have enough pieces
        if (args.length < 2) {
            return `\`.ranges batter; pitcher[; park]\``;
        }

        // Get the pieces
        let batterQuery = args[0].trim();
        let pitcherQuery = args[1].trim();
        let parkQuery = args[2];
        if (parkQuery === undefined)
            parkQuery = 'Neutral Park';

        // Make sure they're all valid
        let pitcher = await getPlayer(pitcherQuery);
        if (typeof pitcher === 'string') {
            return pitcher;
        }

        let batter = await getPlayer(batterQuery);
        if (typeof batter === 'string') {
            return batter;
        }

        let park = await getField(parkQuery.trim());
        if (typeof park === 'string')
            return park;

        // Calculate range
        let calculatedRange = calculateRanges(batter, pitcher, park);

        // Make the embed message format
        let description = '';
        if (pitcher.pitchingType.name === 'Position' && batter.pitchingType.name !== 'Position') // Maybe they got it backwards
            description += `**WARNING:** ${pitcher.name} is a position pitcher. Did you mean:\n\`.swing ${pitcherQuery}; ${batterQuery}; ${parkQuery}\`\n\n`;
        description += `${pitcher.name} pitching against ${batter.name} at ${park.name}.`;
        let ret = new RichEmbed({
            description: description,
            fields: []
        });

        // Calculate diffs using a running total with the ranges,
        // and add the results to the embed
        let runningTotal = 0;
        let ranges = [calculatedRange.rangeHR, calculatedRange.range3B, calculatedRange.range2B, calculatedRange.range1B, calculatedRange.rangeBB, calculatedRange.rangeFO, calculatedRange.rangeK, calculatedRange.rangePO, calculatedRange.rangeRGO, calculatedRange.rangeLGO];
        let outcomes = ["HR", "3B", "2B", "1B", "BB", "FO", "K", "PO", "RGO", "LGO"];
        for (let i = 0; i < ranges.length; i++) {
            ret.addField(outcomes[i], `${runningTotal} - ${runningTotal + ranges[i] - 1}`, true);
            runningTotal += ranges[i];
        }

        return ret;
    }

}

export const RangesCommand = new RangesCommandClass();
