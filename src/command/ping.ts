import {Command} from "./command";
import {Message, RichEmbed, Snowflake, TextChannel, User} from "discord.js";
import {getPlayer} from "../players";
import {getLatestGameThreadsForTeam, getSnowflake, getUserSetting} from "../database";
import {SNOWFLAKE_CHANNEL_AB_PINGS, SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN} from "../important_constants";

class PendingReaction {
    message: Message;
    pingedUser: string;
    pingedSnowflake: Snowflake;
    srcChannel: TextChannel;
    origAuthor: Snowflake;

    constructor(message: Message, pingedUser: string, pingedSnowflake: Snowflake, srcChannel: TextChannel, origAuthor: string) {
        this.message = message;
        this.pingedUser = pingedUser;
        this.pingedSnowflake = pingedSnowflake;
        this.srcChannel = srcChannel;
        this.origAuthor = origAuthor;
    }
}

/**
 * Notifies a player that it's their turn to bat.
 * Only accessible from the #umpires channel.
 */
class PingCommandClass extends Command {

    pendingReactions: PendingReaction[] = [];

    // Hide from other channels
    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (args.length < 1) {
            return `I need a player to ping.`;
        }
        let milrMode = false;
        if (args[0].toLowerCase() === 'milr') {
            args = args.slice(1, args.length);
            milrMode = true;
        }
        return this.handlePingCommand(args, msg, milrMode);
    }

    // noinspection JSMethodCanBeStatic
    async handlePingCommand(args: string[], msg: Message, milrMode: boolean): Promise<string | RichEmbed> {
        // Make sure we have a player name to ping
        if (args.length < 1)
            return `I need a player to ping.`;

        // Find the player, or give them the search failure message if we couldn't find one
        let searchQuery = args.join(' ');
        let player = await getPlayer(searchQuery);
        if (typeof player === 'string')
            return player;

        // Find the discord snowflake of the player to ping, or let them know if we couldn't
        let snowflake = await getSnowflake(player);

        // Check if we can ping the GM instead
        let pingingGM = false;
        if (snowflake === undefined) {
            if (player.team.tag !== 'FA') {
                snowflake = await getSnowflake(player.team.gm);
                pingingGM = true;
            }
        }

        // Let the ump know if we couldn't ping them at all
        if (snowflake === undefined)
            return `That player isn't Discord Verified, so I can't ping them. :cry:`;

        // Now make sure they target being pinged is opted in
        if (!(await getUserSetting(snowflake, pingingGM ? 'ping.gmoptin' : 'ping.optin', true))) {
            if (pingingGM)
                return `That player isn't Discord Verified, and the GM has opted out of receiving team pings.`;
            else
                return `That player has opted out of receiving at-bat pings.`;
        }

        // Create the base message
        let pingMessage = pingingGM ?
            `<@${snowflake}>, ${player.name} is up to bat${milrMode ? ' in MiLR' : ''}, but can't be pinged.` :
            `<@${snowflake}>, you're up to bat${milrMode ? ' in MiLR' : ''}!`;

        let gameID36;
        if (milrMode) {
            // if we can't get the player's milr team, don't include a game thread
            gameID36 = player.team.milrTeam ? (await getLatestGameThreadsForTeam(player.team.milrTeam))[0].id36 : undefined;
        } else {
            gameID36 = (await getLatestGameThreadsForTeam(player.team))[0].id36;
        }

        pingMessage += gameID36 !== undefined ? `\n\nhttps://reddit.com/r/fakebaseball/comments/${gameID36}/` : ``;

        pingMessage += `\n\nTap the baseball below to let the umpire know you've swung.`;

        let targetChannel = msg.guild.id === SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ? msg.guild.channels.get(SNOWFLAKE_CHANNEL_AB_PINGS) as TextChannel : msg.channel;
        let createdMessage = (await targetChannel.send(pingMessage)) as Message;

        // Add the baseball reaction
        await createdMessage.react('⚾');

        // Store for message for when it's reacted to
        this.pendingReactions.push(new PendingReaction(createdMessage, player.name, snowflake, msg.channel as TextChannel, msg.author.id));

        // Let the sender know if it wasn't sent to the same channel
        if (targetChannel.id !== msg.channel.id)
            return pingingGM ?
                `Pinged ${player.team.gm.name} for ${player.name}!` :
                `Pinged ${player.name}!`;

        return undefined;
    }

    async handlePingReaction(reactedMsg: Message, user: User) {
        for (let [index, pending] of this.pendingReactions.entries()) {
            if (pending.message.id === reactedMsg.id && pending.pingedSnowflake === user.id) {
                this.pendingReactions.splice(index, 1);
                if (await getUserSetting(pending.origAuthor, 'ping.umpoptin', true))
                    await pending.srcChannel.send(`<@${pending.origAuthor}>, ${pending.pingedUser} has swung!`);
                break;
            }
        }
    }

}

export const PingCommand = new PingCommandClass();
