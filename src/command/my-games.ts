import {Command} from "./command";
import {Message, RichEmbed} from "discord.js";
import {getLatestGameThreadsForTeam, getPlayer as getPlayerFromDB} from "../database";
import * as Discord from "discord.js";



/**
 * Gives the user the reddit links of the games their teams are playing in
 */
class MyGamesCommandClass extends Command {

    public async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {

        // Check the DB for the current user's profile,
        // or tell them to provide a name/claim their account.
        let player = await getPlayerFromDB(msg.author.id);
        if (player === undefined)
            return `I don't know who you are yet. Use .claim to link your Discord!`;

        let searchNum = 1;

        //Clamp number between 1-10 if they input something
        if (args.length > 0){
            searchNum = parseInt(args[0]);

            if (isNaN(searchNum) || searchNum < 1)
                searchNum = 1;
            else if (searchNum > 10)
                searchNum = 10;
        }
        

        //Get all of the information about that player's team's games in mlr and milr
        let mlrGames = await getLatestGameThreadsForTeam(player.team, searchNum);
        let milrGames = player.team.milrTeam ? await getLatestGameThreadsForTeam(player.team.milrTeam, searchNum) : undefined;

        if (mlrGames === undefined && milrGames === undefined)
        {
            return `I couldn't find any games for you.`
        }

        // Now that we have the game threads, we create the embed fields with all the info
        let fields = [];
        if (mlrGames !== undefined)
        {
            fields.push({
                inline: false,
                name: this.CreateTitleOfGames(player.team.name,mlrGames.length),
                value: this.CreateListOfGamesMessage(mlrGames)
            });
        }
        if (milrGames !== undefined)
        {
            fields.push({
                inline: false,
                name: this.CreateTitleOfGames(player.team.milrTeam.name,milrGames.length),
                value: this.CreateListOfGamesMessage(milrGames)
            });
        }
        
        // Now create the embed and send it
        return new Discord.RichEmbed({
            title: `${player.name}'s games`,
            color: player.team.color,
            fields: fields,
        });

    }

    //Create the string fields of every game for that team
    private CreateListOfGamesMessage(games:any[]): string {
        
        let msg =``;

        for (let i = 0; i < games.length; i++)
        {
            msg += `${games[i].away_team} vs. ${games[i].home_team} : https://reddit.com/r/fakebaseball/comments/${games[i].id36}/\n`;
        }

        return msg;
    }

    //Creates the title such as "Marlins last 5 games". Used mostly for plural vocabulary
    private CreateTitleOfGames(teamName:string, numberOfGames:number): string {
        
        if (numberOfGames <= 0)
            return ``;
        else if (numberOfGames == 1)
            return `${teamName}'s last game.`;
        else
            return `${teamName}'s last ${numberOfGames} games`;
        
    }
        
}


export const MyGamesCommand = new MyGamesCommandClass();

