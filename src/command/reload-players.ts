import {Command} from "./command";
import {GuildChannel, Message} from "discord.js";
import {DEVELOPMENT_MODE, SNOWFLAKE_ROLE_OOTC, SNOWFLAKE_USER_LLAMOS} from "../important_constants";
import {loadPlayers} from "../players";

class ReloadPlayersCommandClass extends Command {

    lastReload: number = 0;

    async handleCommand(args: string[], msg: Message): Promise<string> {
        if ((!DEVELOPMENT_MODE || !(msg.channel instanceof GuildChannel) || !msg.member.roles.has(SNOWFLAKE_ROLE_OOTC)) && msg.author.id !== SNOWFLAKE_USER_LLAMOS)
            return `You aren't allowed to do that.`;

        // Be careful of rate limiting
        if (Date.now() - this.lastReload < 30000)
            return `It's been too soon since the last reload.`;

        // Reload all the players
        await loadPlayers();
        this.lastReload = Date.now();
        return `Reloaded.`;
    }
}

export const ReloadPlayersCommand = new ReloadPlayersCommandClass();
