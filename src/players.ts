import * as rp from 'request-promise';
import {BattingType, PitchingType, PitchingBonus, Field} from "./SwingStat";

// All the ranges that we'll pull data from
const BATTING_TYPES_RANGE = 'Tables!AG2:AR14';
const PITCHER_TYPES_RANGE = 'Tables!AG17:AR30';
const PITCHER_BONUSES_RANGE = 'Tables!AG33:AR35';
const FIELDS_RANGE = "'MLR Parks'!A4:G23";
const TEAMS_RANGE = 'Tables!T53:AF76';
const MILR_TEAMS_RANGE = 'Tables!T80:Y87';
const PLAYERS_RANGE = 'Tables!F1:Q600';

// The big papa spreadsheet with everything we ever need
const SPREADSHEET_URL = `https://sheets.googleapis.com/v4/spreadsheets/1JxXUlVbF_c72OA_f9wdykNNkJ0k4Z49vsUl8J-qNzJA/values:batchGet?ranges=${BATTING_TYPES_RANGE}&ranges=${PITCHER_TYPES_RANGE}&ranges=${PITCHER_BONUSES_RANGE}&ranges=${encodeURIComponent(FIELDS_RANGE)}&ranges=${MILR_TEAMS_RANGE}&ranges=${TEAMS_RANGE}&ranges=${PLAYERS_RANGE}&key=${process.env.GOOGLE_API_KEY}`;

/**
 * Represents one team's data.
 * Fields are represented by their name, since tbe non-team teams (FA and MiLR)
 * are created before the fields can be initialized.
 */
export class Team {
    name: string;
    tag: string;
    color: number;
    field: string;
    milrTeam: Team;
    gm: Player;

    constructor(name: string, tag: string, color: string, field: string, milrTeam: Team) {
        this.name = name;
        this.tag = tag;
        this.color = parseInt(color, 16);
        this.field = field;
        this.milrTeam = milrTeam;
    }
}

/**
 * Represents one player's data.
 * Snowflakes are not stored here, but can be found with database.getSnowflake
 */
export class Player {
    name: string;
    team: Team;
    redditName: string;
    discordName: string;
    battingType: BattingType;
    pitchingType: PitchingType;
    pitchingBonus: PitchingBonus;
    rightHanded: boolean;
    primaryPos: string;
    secondaryPos: string;
    tertiaryPos: string;

    constructor(gm: boolean, name: string, team: Team, redditName: string, discordName: string, battingType: string, pitchingType: string, pitchingBonus: string, handedness: string, primaryPos: string, secondaryPos: string, tertiaryPos: string) {
        this.name = name.trim();
        this.team = team;
        this.redditName = redditName.trim();
        this.discordName = discordName.trim();
        this.battingType = battingTypes[battingType.trim()];
        this.pitchingType = pitchingTypes[pitchingType.trim()];
        this.pitchingBonus = pitchingBonuses[pitchingBonus.trim()];
        this.rightHanded = handedness.trim().toLowerCase() === "right";
        this.primaryPos = primaryPos;
        this.secondaryPos = secondaryPos;
        this.tertiaryPos = tertiaryPos;

        // Check for common invalid entries
        if (this.battingType === undefined)
            this.battingType = battingTypes['BN'];
        if (this.pitchingType === undefined)
            this.pitchingType = pitchingTypes['POS'];
        if (this.pitchingBonus === undefined)
            this.pitchingBonus = pitchingBonuses['B'];
        if (this.primaryPos === undefined || this.primaryPos === 'Discord') // This one shouldn't happen but who knows
            this.primaryPos = "None";
        if (this.secondaryPos === undefined || this.secondaryPos === 'Discord')
            this.secondaryPos = "None";
        if (this.tertiaryPos === undefined || this.tertiaryPos === 'Discord')
            this.tertiaryPos = "None";

        if (gm)
            team.gm = this;
    }
}

const battingTypes = {};
const pitchingTypes = {};
const pitchingBonuses = {};
const players: Player[] = [];
const fields: Field[] = [];
const teams: Team[] = [];

// Create base park with no modifications
let neutralPark = new Field('Neutral Park', 1, 1, 1, 1, 1);


// Create dummy teams
teams.push(new Team("Free Agents", "FA", "FFFFFF", "Neutral Park", null));
//teams.push(new Team("Minor League Redditball", "MiLR", "FFFFFF", "The 'Foam Doam"));

/**
 * Finds a player using their reddit name.
 * Must be an exact match.
 * @param search The reddit name to search for.
 */
export async function getPlayerByRedditName(search: string): Promise<Player | undefined> {
    // Load info from sheet if that hasn't happened yet
    if (players.length === 0) {
        await loadPlayers();
    }

    // Make sure the search string is actually defined
    // (this can happen from the database file)
    if (search === undefined)
        return undefined;

    // Go lower case for case-insensitivity
    search = search.toLowerCase();

    // Aggregate all players that have an exact match reddit name
    let matches: Player[] = [];
    for (let player of players) {
        if (player.redditName.toLowerCase() === search)
            matches.push(player);
    }

    // Only one player should be returned, so log an error of the duplicate
    if (matches.length > 1) {
        console.error(`Duplicate reddit name: ${search}`);
        return undefined;
    }

    // Return the player if found, or undefined
    if (matches.length === 0)
        return undefined;

    return matches[0];
}

/**
 * Searches for a player by their Discord name.
 * This Discord name used is the one on the sheet, even if the player is verified.
 * Must be an exact match.
 * @param nameWithDiscriminator The username#discriminator to search.
 */
export async function getPlayerByDiscord(nameWithDiscriminator: string): Promise<Player | undefined> {
    // Load info from sheet if that hasn't happened yet
    if (players.length === 0)
        await loadPlayers();

    // Go lower case for case-insensitivity
    nameWithDiscriminator = nameWithDiscriminator.toLowerCase();

    // Aggregate all players that have an exact match Discord name
    let matches: Player[] = [];
    for (let player of players) {
        if (player.discordName.toLowerCase() === nameWithDiscriminator)
            matches.push(player);
    }

    // Only one player should be returned, so log an error of the duplicate
    if (matches.length > 1) {
        console.error(`Duplicate reddit name: ${nameWithDiscriminator}`);
        return undefined;
    }

    // Return the player if found, or undefined
    if (matches.length === 0)
        return undefined;

    return matches[0];
}

/**
 * Searches for a player based on Reddit name, Discord name or player name.
 * Exact matches get priority, but any player that contains the search string
 * will match as well.
 * @param search The search query.
 * @return Iff exactly one player is found, the player instance. Otherwise, a message with more info.
 */
export async function getPlayer(search: string): Promise<string | Player> {
    // Load info from sheet if that hasn't happened yet
    if (players.length === 0) {
        await loadPlayers();
    }

    // Go lower case for case-insensitivity
    search = search.toLowerCase();

    // Aggregate all players with matching names
    let matches: Player[] = [];
    for (let player of players) {
        // Double check prevents issues when one name is a subset of another
        // Exact match is quick return
        if (player.name.toLowerCase() === search || player.discordName.toLowerCase() === search || player.redditName.toLowerCase() === search) {
            return player;
        }
        // Partial match waits to check all players
        if (player.name.toLowerCase().includes(search) || player.discordName.toLowerCase().includes(search) || player.redditName.toLowerCase().includes(search)) {
            matches.push(player);
        }
    }

    // If we found too many players, let the searcher now
    if (matches.length > 5) {
        return `Your search for ${search} returned more than 5 players. Please be more specific.`;
    }

    // If we found a few, let them know who we found
    if (matches.length > 1) {
        return `Your search for ${search} returned multiple players:\n    ${matches.map(p => p.name).join('\n    ')}`;
    }

    // Return the player if we only found one
    if (matches.length === 1) {
        return matches[0];
    }

    // Let them know if nothing was found
    return `Your search for ${search} returned no players.`
}

/**
 * Searches for a field based on team tag or field name.
 * Team tag must be an exact match,
 * but field name can be partial.
 * @param search The tag or search query.
 * @return The field if found, otherwise a message with more info.
 */
export async function getField(search: string): Promise<string | Field>  {
    // Load info from sheet if that hasn't happened yet
    if (players.length === 0) {
        await loadPlayers();
    }

    // Go lower case for case-insensitivity
    search = search.toLowerCase();

    // Check if they entered a team Tag
    let team = await getTeam(search) as Team;
    if (typeof team !== 'string') {
        search = team.field.toLowerCase();

        //If they have no stadium according to their table, return our neutral park.
        if (search === "no stadium")
            return neutralPark;
        
    }
    

    // Aggregate all the names with matches
    let matches: Field[] = [];
    for (let field of fields) {
        // Double check prevents issues when one name is a subset of another (Dodger Stadium)
        if (field.name.toLowerCase() === search)
            return field;
        if (field.name.toLowerCase().includes(search))
            matches.push(field);
    }

    // If we found too many, let the searcher know
    if (matches.length > 5) {
        return `Your search for ${search} returned more than 5 fields. Please be more specific.`;
    }

    // If we found a few, let the searcher know which ones
    if (matches.length > 1) {
        return `Your search for ${search} returned multiple fields:\n    ${matches.map(p => p.name).join('\n    ')}`;
    }

    // Return the field if we only found one
    if (matches.length === 1) {
        return matches[0];
    }

    // Otherwise tell the searcher we found nothing
    return `Your search for ${search} returned no fields.`
}

/**
 * Searches for a team based on team tag or team name.
 * Both can be partial matches.
 * @param search
 */
export async function getTeam(search: string) {
    // Load info from sheet if that hasn't happened yet
    // FA team exists from the start, hence checking for size 1
    if (teams.length === 1) {
        await loadPlayers();
    }

    // Go lower case for case-insensitivity
    search = search.toLowerCase();

    // Aggregate all matching teams
    let matches: Team[] = [];
    for (let team of teams) {
        // Double check prevents issues when one name is a subset of another
        if (team.name.toLowerCase() === search || team.tag.toLowerCase() === search)
            return team;
        if (team.name.toLowerCase().includes(search) || team.tag.toLowerCase() === search)
            matches.push(team);
    }

    // If we found too many, let the searcher know
    if (matches.length > 5) {
        return `Your search for ${search} returned more than 5 teans. Please be more specific.`;
    }

    // If we found a few, let the searcher know which ones
    if (matches.length > 1) {
        return `Your search for ${search} returned multiple teams:\n    ${matches.map(p => p.name).join('\n    ')}`;
    }

    // Return the team if we only found one
    if (matches.length === 1) {
        return matches[0];
    }

    // Otherwise tell the searcher we found nothing
    return `Your search for ${search} returned no teams.`
}

/**
 * Loads all the information from the spreadsheet.
 */
export async function loadPlayers() {
    // Clear out arrays in case this is a reload
    fields.splice(0, fields.length);
    teams.splice(0, teams.length);
    players.splice(0, players.length);

    //Add neutral park back to the fields
    fields.push(neutralPark);

    // Do the request to the Google Sheets API
    let reqOptions = {
        uri: SPREADSHEET_URL,
        json: true
    };
    let bigData = await rp(reqOptions);

    // Grab the returned values
    let tables = bigData.valueRanges;
    for (let table of tables) {
        if (table['range'] === BATTING_TYPES_RANGE) { // Get batting types
            for (let row of table['values']) {
                battingTypes[row[0]] = new BattingType(row[1], parseInt(row[2]), parseInt(row[3]), parseInt(row[4]), parseInt(row[5]), parseInt(row[6]), parseInt(row[7]), parseInt(row[8]), parseInt(row[9]), parseInt(row[10]), parseInt(row[11]));
            }
            console.log(`Loaded batting types.`);
        } else if (table['range'] === PITCHER_TYPES_RANGE) { // Get pitcher types
            for (let row of table['values']) {
                pitchingTypes[row[0]] = new PitchingType(row[1], parseInt(row[2]), parseInt(row[3]), parseInt(row[4]), parseInt(row[5]), parseInt(row[6]), parseInt(row[7]), parseInt(row[8]), parseInt(row[9]), parseInt(row[10]), parseInt(row[11]));
            }
            console.log(`Loaded pitching types.`);
        } else if (table['range'] === PITCHER_BONUSES_RANGE) { // Get pitcher bonuses
            for (let row of table['values']) {
                pitchingBonuses[row[0]] = new PitchingBonus(row[1], parseInt(row[2]), parseInt(row[3]), parseInt(row[4]), parseInt(row[5]), parseInt(row[6]), parseInt(row[7]), parseInt(row[8]), parseInt(row[9]), parseInt(row[10]), parseInt(row[11]));
            }
            console.log(`Loaded pitching bonuses.`);
        } else if (table['range'] === FIELDS_RANGE) { // Get fields
            for (let row of table['values']) {
                fields.push(new Field(row[1], parseFloat(row[6]), parseFloat(row[5]), parseFloat(row[4]), parseFloat(row[2]), parseFloat(row[3])));
            }
            console.log(`Loaded fields.`);
        } else if (table['range'] === MILR_TEAMS_RANGE) { // Get MiLR teams; this needs to run before the MLR teams so they populate correctly
            for (let row of table['values']) {
                teams.push(new Team(row[0], row[2], row[5], `The 'Foam Doam`, null));
            }
            console.log(`Loaded MiLR teams.`);
        } else if (table['range'] === TEAMS_RANGE) { // Get teams
            for (let row of table['values']) {
                // should have a milr team but no biggie if they don't
                let milrTeam = await getTeam(row[12]);
                teams.push(new Team(row[0], row[2], row[11], row[3], typeof milrTeam === 'string' ? undefined : milrTeam));
            }
            console.log(`Loaded teams.`);
        } else if (table['range'] === PLAYERS_RANGE) { // Get players
            for (let row of table['values']) {
                if (row.length < 10)
                    continue;
                players.push(new Player(row[0] === 'GM', row[1], await getTeam(row[2]) as Team, row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11]));
            }
            console.log(`Loaded ${players.length} players.`);
        }
    }
}
