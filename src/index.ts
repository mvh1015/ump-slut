import * as Discord from 'discord.js';
import {RichEmbed} from 'discord.js';
import * as RedditWrapper from 'snoowrap';
import {PlayerCommand} from './command/player';
import {Command} from "./command/command";
import {SwingCommand} from "./command/swing";
import {GameSheetCommand, HandbookCommand, RulebookCommand} from "./command/link-commands";
import {claimRequest} from "./reddit_claim";
import {ClaimCommand} from "./command/claim";
import {PingCommand} from "./command/ping";
import {RangesCommand} from "./command/ranges";
import {getPlayerByDiscord} from "./players";
import {claimUser, getPlayer} from "./database";
import {AddGameCommand} from "./command/add-game";
import {MyGamesCommand} from "./command/my-games";
import {
    PingGMOptInCommand,
    PingGMOptOutCommand,
    PingOptInCommand,
    PingOptOutCommand,
    UmpPingBackOptInCommand,
    UmpPingBackOptOutCommand
} from "./command/ping-optin-commands";
import {
    DEVELOPMENT_MODE,
    EMBED_FOOTER,
    SNOWFLAKE_CHANNEL_UMP_PINGS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
    SNOWFLAKE_USER_LLAMOS
} from "./important_constants";
import {ReloadPlayersCommand} from "./command/reload-players";
import {InviteCommand} from "./command/invite";

const DISCORD_TOKEN = process.env.DISCORD_TOKEN;
const discordClient = new Discord.Client();
const prefix = '.';

// Might need to regenerate and hide the client secret later,
// but for now the reddit client is a script,
// so it only has access to the SluttyUmps account anyway
const reddit = new RedditWrapper({
    userAgent: 'UmpSlut/1.0',
    clientId: 'QdYHACjnVJ0bpQ',
    clientSecret: 'vniodWCqh_QVI-QulpTx3dNRfM4',
    username: 'SluttyUmps',
    password: process.env.REDDIT_PASSWORD
});

// Log when we make a connection to Discord
discordClient.on('ready', () => {
    console.log("Logged in!");
});

// Maps the command names to the command instances
const commandMap = {
    player: PlayerCommand,
    swing: SwingCommand,
    handbook: HandbookCommand,
    gamesheet: GameSheetCommand,
    rulebook: RulebookCommand,
    claim: ClaimCommand,
    ping: PingCommand,
    ranges: RangesCommand,
    addgame: AddGameCommand,
    mygames: MyGamesCommand,
    pingoptin: PingOptInCommand,
    pingoptout: PingOptOutCommand,
    gmpingoptin: PingGMOptInCommand,
    gmpingoptout: PingGMOptOutCommand,
    umppingoptout: UmpPingBackOptOutCommand,
    umppingoptin: UmpPingBackOptInCommand,
    reloadplayers: ReloadPlayersCommand,
    invite: InviteCommand,
};

discordClient.on('message', async msg => {
    if (!msg.content.startsWith(prefix) || msg.author.bot) {
        return;
    }

    const args = msg.content.slice(prefix.length).split(/ +/);
    const commandLabel = args.shift().toLowerCase();

    // Find command to call
    let command: Command = commandMap[commandLabel];

    // Make sure it isn't a hidden one called in the wrong channel
    if (command !== undefined) {
        if ((command.hidden() && msg.channel.id !== SNOWFLAKE_CHANNEL_UMP_PINGS) && !DEVELOPMENT_MODE)
            return;
    }

    // Now do nothing if we don't know the command
    if (command === undefined)
        return;

    try {
        // Call the command
        let response = await command.handleCommand(args, msg);
        if (response === undefined)
            return;

        // Append the "If something's wrong" footer if it's an Embed
        if (response instanceof RichEmbed) {
            response.footer = EMBED_FOOTER;
        }

        // Send the response
        msg.channel.send(response)
            .catch(console.error);
    } catch (e) {
        // DMs me (Llamos) when there's an unhandled error in a command on production.
        if (!DEVELOPMENT_MODE) {
            discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).members.get(SNOWFLAKE_USER_LLAMOS).send('An error in console for you, sir.').catch(console.error);
            console.error(e);
            msg.channel.send(`Something went wrong. I've already notified Llamos (the author), so please try again later.`)
                .catch(console.error);
        }
    }
});

// Login to Discord, starts background sockets
discordClient.login(DISCORD_TOKEN)
    .catch(console.error);

// When a player joins, attempt to claim them.
// If that fails, send them a prompt to self-claim.
discordClient.on('guildMemberAdd', async member => {
    // In dev mode, do nothing
    if (DEVELOPMENT_MODE)
        return;

    // Check if they're already claimed (probably a returning member)
    let claimedPlayer = await getPlayer(member.id);
    if (claimedPlayer !== undefined)
        return;

    // If they didn't exist in the DB, try to find them from the sheet
    let player = await getPlayerByDiscord(`${member.user.username}#${member.user.discriminator}`);

    // If we couldn't find them, send them a prompt to self-claim
    if (player === undefined) {
        member.send(`Hi! Welcome to /r/fakebaseball's Discord server. I couldn't recognize your Discord name.\n\nIf you somehow ended up here and haven't heard of fakebaseball, please check out https://reddit.com/r/fakebaseball\n\nIf you are a player, please type \`.claim [player name]\` to begin the Discord linking process.`);
        return;
    }

    // Otherwise, claim them now.
    await claimUser(player, member.id, 'AutoOnJoin');
});

console.log(`Development Mode: ${DEVELOPMENT_MODE ? 'Yes' : 'No'}`);

discordClient.on('messageReactionAdd', async (messageReaction, user) => {
    // Make sure the bot wrote the message to be reacted to
    if (messageReaction.message.author.id === discordClient.user.id) {
        // For baseball reaction, should be .ping
        if (messageReaction.emoji.name === '⚾') {
            await commandMap.ping.handlePingReaction(messageReaction.message, user);
        }
    }
});

// Reddit section
if (!DEVELOPMENT_MODE)
    setInterval(() => checkDMs().catch(console.error), 60000);

// Checks for new reddit-verification messages
async function checkDMs() {
    // Pull all the unread messages in the inbox
    let messages = (await reddit.getUnreadMessages()).fetchAll();
    messages.forEach(message => {
        // Make sure it's a verification message
        if (message.subject === 'VERIFY') {
            // Mark it read so we don't re-check it
            message.markAsRead().catch(console.error);
            // Attempt to claim the code and respond with success or fail message
            claimRequest(`/u/${message.author.name}`, message.body)
                .then(response => message.reply(response))
                .catch(console.error);
        }
    });
}

