import {Snowflake} from "discord.js";

export const SNOWFLAKE_USER_LLAMOS: Snowflake = '138318226181324800';
export const SNOWFLAKE_ROLE_GM: Snowflake = '344859708022194176';
export const SNOWFLAKE_ROLE_OOTC: Snowflake = '344860182053912576';
export const SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN: Snowflake = '344859525582422016';
export const SNOWFLAKE_CHANNEL_UMP_PINGS: Snowflake = '573242415671017493';
export const SNOWFLAKE_CHANNEL_AB_PINGS: Snowflake = '572841324391170059';

export const DEVELOPMENT_MODE: boolean = parseInt(process.env.DEVELOPMENT) === 1;

export const REDDIT_ID36_FROM_URL_REGEX = /https:?\/\/.*?\/r\/fakebaseball\/comments\/([0-9a-z]{6,7}).*?/gi;

export const EMBED_FOOTER = {
    icon_url: 'https://cdn.discordapp.com/avatars/138318226181324800/cbf6258dde20e6d276e472fb4fef6b7e.png',
    text: 'If something seems wrong, please message Llamos#0001.'
};
