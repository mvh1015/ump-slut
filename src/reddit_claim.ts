import {randomBytes} from "crypto";
import {getPlayerByRedditName} from "./players";
import {claimUser} from "./database";

/**
 * Represents one request to claim a snowflake to a reddit account.
 */
export class ClaimRequest {
    snowflake: string;
    redditName: string;

    constructor(snowflake: string, redditName: string) {
        this.snowflake = snowflake;
        this.redditName = redditName;
    }
}

/**
 * Represents a ClaimRequest with the claim code and timestamp.
 */
class ClaimReqWithCode {
    code: string;
    req: ClaimRequest;
    timestamp: number;

    constructor(code: string, req: ClaimRequest) {
        this.code = code;
        this.req = req;
        this.timestamp = Date.now();
    }
}

let requests: ClaimReqWithCode[] = [];

/**
 * Generates a string of 32 base64-encoded random bytes
 */
function generateClaimCode(): string {
    return randomBytes(32).toString('base64')
}

/**
 * Creates a new claim for a Discord snowflake to Reddit account.
 * This method will generate the unique claim code.
 * @param claimRequest The claim request to create.
 * @return The generated claim code to provide to the user.
 */
export function makeClaimRequest(claimRequest: ClaimRequest): string {
    // Create claim
    let claimCode = generateClaimCode();
    let association = new ClaimReqWithCode(claimCode, claimRequest);

    // Add it to the list of active claims
    requests.push(association);
    return claimCode;
}

/**
 * Claims a ClaimRequest using the provided claim code.
 * @param redditName The reddit account claiming the code.
 * @param claimCode The code to be claimed.
 * @return A message with the result of the claim.
 */
export async function claimRequest(redditName: string, claimCode: string): Promise<string> {
    // Prune the current list of oldies (+5 min)
    requests = requests.filter(req => req.timestamp > Date.now() - 300000);

    // Get the claim
    let request = requests.filter(req => req.code === claimCode).pop() as ClaimReqWithCode;
    if (request === undefined) {
        console.log(`Claim: ${redditName} submitted an invalid claim code: ${claimCode}`);
        return `This claim code wasn't found. This can happen if your code expired (5 minutes) or I've been restarted within the time you received the code and the time I saw this message. If this persists, please message Llamos#0001 on Discord.\n\nPlease try again.`
    }

    // Make sure it's legit
    if (request.req.redditName.toLowerCase() !== redditName.toLowerCase()) {
        console.log(`Claim: ${redditName} submitted a claim code that was meant for ${request.req.redditName}`);
        return `You must send the verification code from the same reddit account that is registered on fakebaseball to the player you want to claim.`;
    }

    // Find the player
    let player = await getPlayerByRedditName(redditName);
    if (player === undefined) {
        console.error(`Claim: Couldn't find player by reddit name ${redditName}`);
        return `I can't find this reddit account registered on /r/fakebaseball. This shouldn't happen so please message Llamos#0001 on Discord.`;
    }

    // Now complete the claim
    try {
        await claimUser(player, request.req.snowflake, 'Reddit');
    } catch (e) {
        console.error(`Claim: Failed to claim user. Snowflake: ${request.req.snowflake} Reddit: ${request.req.redditName}\n${e}`);
        return `I ran into an error while trying to claim your account name. This shouldn't happen so please message Llamos#0001 on Discord.`;
    }

    // Let them know
    return `Your account has been successfully claimed. Thank you!`
}
