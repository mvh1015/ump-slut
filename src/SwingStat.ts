import {Player} from "./players";

/**
 * Represents a set of numbers used in swing calculation.
 * Each outcome is assigned a range, and is combined linearly.
 */
export class SwingStat {
    name: string;
    rangeHR: number;
    range3B: number;
    range2B: number;
    range1B: number;
    rangeBB: number;
    rangeFO: number;
    rangeK: number;
    rangePO: number;
    rangeRGO: number;
    rangeLGO: number;

    constructor(name: string, rangeHR: number, range3B: number, range2B: number, range1B: number, rangeBB: number, rangeFO: number, rangeK: number, rangePO: number, rangeRGO: number, rangeLGO: number) {
        this.name = name;
        this.rangeHR = rangeHR;
        this.range3B = range3B;
        this.range2B = range2B;
        this.range1B = range1B;
        this.rangeBB = rangeBB;
        this.rangeFO = rangeFO;
        this.rangeK = rangeK;
        this.rangePO = rangePO;
        this.rangeRGO = rangeRGO;
        this.rangeLGO = rangeLGO;
    }

    /**
     * Combines two ranges by summation.
     * @param otherStat The other SwingStat to add to the current.
     * @return The new, calculated SwingStat.
     */
    add(otherStat: SwingStat) {
        return new SwingStat(
            "Calculated",
            this.rangeHR + otherStat.rangeHR,
            this.range3B + otherStat.range3B,
            this.range2B + otherStat.range2B,
            this.range1B + otherStat.range1B,
            this.rangeBB + otherStat.rangeBB,
            this.rangeFO + otherStat.rangeFO,
            this.rangeK + otherStat.rangeK,
            this.rangePO + otherStat.rangePO,
            this.rangeRGO + otherStat.rangeRGO,
            this.rangeLGO + otherStat.rangeLGO
        )
    }

    /**
     * Generates a table-like output of the SwingStat
     * with each column padded to length 4
     * and delimited by vertical pipes (|).
     */
    toString() {
        return `|${this.rangeHR.toString().padEnd(4, ' ')}|${this.range3B.toString().padEnd(4, ' ')}|${this.range2B.toString().padEnd(4, ' ')}|${this.range1B.toString().padEnd(4, ' ')}|${this.rangeBB.toString().padEnd(4, ' ')}|${this.rangeFO.toString().padEnd(4, ' ')}|${this.rangeK.toString().padEnd(4, ' ')}|${this.rangePO.toString().padEnd(4, ' ')}|${this.rangeRGO.toString().padEnd(4, ' ')}|${this.rangeLGO.toString().padEnd(4, ' ')}|`
    }
}

// These 3 have no special behaviour
export class BattingType extends SwingStat {}
export class PitchingBonus extends SwingStat {}
export class PitchingType extends SwingStat {}

export class Field extends SwingStat {

    constructor(name: string, rangeHR: number, range3B: number, range2B: number, range1B: number, rangeBB: number) {
        super(name, rangeHR, range3B, range2B, range1B, rangeBB, 0, 0, 0, 0, 0)
    }

    /**
     * Calculates the field adjustments by weighting each result with the field stats.
     * @param otherStat The other SwingStat to base the calculations off of.
     * @return The new, calculated, linear adjustments.
     */
    calculateAdjustments(otherStat: SwingStat) {
        // Calculate differences by field factors
        let adjustments = new SwingStat(
            "Calculated",
            Math.round(this.rangeHR * otherStat.rangeHR) - otherStat.rangeHR,
            Math.round(this.range3B * otherStat.range3B) - otherStat.range3B,
            Math.round(this.range2B * otherStat.range2B) - otherStat.range2B,
            Math.round(this.range1B * otherStat.range1B) - otherStat.range1B,
            Math.round(this.rangeBB * otherStat.rangeBB) - otherStat.rangeBB,
            0,
            0,
            0,
            0,
            0,
        );

        // Count the total adjustments already made
        let adjTotal = adjustments.rangeHR + adjustments.range3B + adjustments.range2B + adjustments.range1B + adjustments.rangeBB;

        // Prepare for loop (we need to evenly distribute the negative of adjTotal to the other outcomes)
        let decrement = adjTotal < 0 ? 1 : -1;
        let index = -1;

        // Until we have offset the first adjustments made,
        while (adjTotal != 0) {
            // Cycle through the stats
            index++;
            index %= 5;

            // Get the current accumulative adjustment to the outcome
            let currentAdjustment = 0;
            switch (index) {
                case 0:
                    currentAdjustment = otherStat.rangeFO + adjustments.rangeFO;
                    break;
                case 1:
                    currentAdjustment = otherStat.rangeK + adjustments.rangeK;
                    break;
                case 2:
                    currentAdjustment = otherStat.rangePO + adjustments.rangePO;
                    break;
                case 3:
                    currentAdjustment = otherStat.rangeRGO + adjustments.rangeRGO;
                    break;
                case 4:
                    currentAdjustment = otherStat.rangeLGO + adjustments.rangeLGO;
                    break;
            }

            // Make sure we aren't turning it negative
            if (currentAdjustment <= 0)
                continue;

            // Increase/decrease the outcome range
            adjTotal += decrement;
            switch (index) {
                case 0:
                    adjustments.rangeFO += decrement;
                    break;
                case 1:
                    adjustments.rangeK += decrement;
                    break;
                case 2:
                    adjustments.rangePO += decrement;
                    break;
                case 3:
                    adjustments.rangeRGO += decrement;
                    break;
                case 4:
                    adjustments.rangeLGO += decrement;
                    break;
            }
        }

        // Return the calculated adjustments
        return adjustments;
    }
}

/**
 * Calculates the ranges of a batter against a pitcher in a given field.
 * @param batter The batter player.
 * @param pitcher The pitcher player.
 * @param field The field.
 */
export function calculateRanges(batter: Player, pitcher: Player, field: Field): SwingStat {
    // Calculate the batter/pitcher values
    let pitcherValues = pitcher.pitchingType;
    let batterValues = batter.battingType;
    let calculatedRange = pitcherValues.add(batterValues);

    // Check for hand bonus
    if (pitcher.pitchingType.name !== 'Position' && pitcher.rightHanded == batter.rightHanded) {
        calculatedRange = calculatedRange.add(pitcher.pitchingBonus);
    }

    // Do park adjustments
    if (field.name != 'Neutral Park') {
        let parkAdjustments = field.calculateAdjustments(calculatedRange);
        calculatedRange = calculatedRange.add(parkAdjustments);
    }

    // console.log('                | HR | 3B | 2B | 1B | BB | FO | K  | PO |RGO |LGO |');
    // console.log('Pitcher:'.padEnd(16, ' ') + pitcherValues.toString());
    // console.log('Batter:'.padEnd(16, ' ') + batterValues.toString());
    // if (pitcher.pitchingType.name !== 'Position' && pitcher.rightHanded == batter.rightHanded)
    //     console.log('Hand Bonus:'.padEnd(16, ' ') + pitcher.pitchingBonus.toString());
    // if (field.name != 'Neutral Park') {
    //     let parkAdjustments = field.calculateAdjustments(calculatedRange);
    //     console.log('Park Factors:'.padEnd(16, ' ') + field.toString());
    //     console.log('Park:'.padEnd(16, ' ') + parkAdjustments.toString());
    // }
    // console.log('Final:'.padEnd(16, ' ') + calculatedRange.toString());

    // Return calculated ranges
    return calculatedRange;
}
